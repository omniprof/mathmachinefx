package com.cejv416.mathmachinefx;

import java.text.NumberFormat;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.stage.Stage;
import javafx.scene.layout.GridPane;

import javafx.scene.Parent;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.ColumnConstraints;
import javafx.scene.layout.HBox;

public class MathMachineFX extends Application {

    private TextField operand1;
    private TextField operand2;
    private TextField result;

    @Override
    public void start(Stage primaryStage) {

        Parent root = createLayout();
        Scene scene = new Scene(root, 425, 300);

        primaryStage.setTitle("Math Machine");
        primaryStage.setScene(scene);
        primaryStage.show();
    }

    /**
     * Create the layout in a GridPane container
     *
     * @return the filled in GridPane
     */
    private GridPane createLayout() {
        GridPane layout = new GridPane();

        // Column 0, Row 0
        Label title = new Label("Math Machine");
        title.setStyle("-fx-font-size:14pt; -fx-font-weight:bold; -fx-font-family:Century Gothic, sans-serif");
        // HBox that will span 2 columns so that label can be centered across the GridPane
        HBox hbox = new HBox();
        // Add the label to the HBox
        hbox.getChildren().addAll(title);
        // Center the contents of the HBox
        hbox.setAlignment(Pos.CENTER);
        // Spacing around the controls in the HBox
        hbox.setPadding(new Insets(20.0));
        // Place the HBOX in column 0, row 0, spanning 2 columns and 1 row
        layout.add(hbox, 0, 0, 2, 1);

        // Column 0, Row 1
        Label operand1Label = new Label("Operand 1:");
        operand1Label.setStyle("-fx-font-size:14pt; -fx-font-weight:bold; -fx-font-family:Century Gothic, sans-serif");
        layout.add(operand1Label, 0, 1);

        // Column 1, Row 1
        operand1 = new TextField();
        operand1.setStyle("-fx-font-size:14pt; -fx-font-weight:normal; -fx-font-family:Century Gothic, sans-serif");
        operand1.setAlignment(Pos.CENTER_RIGHT);
        layout.add(operand1, 1, 1);

        // Column 0, Row 2
        Label operand2Label = new Label("Operand 2:");
        operand2Label.setStyle("-fx-font-size:14pt; -fx-font-weight:bold; -fx-font-family:Century Gothic, sans-serif");
        layout.add(operand2Label, 0, 2);

        // Column 1, Row 2
        operand2 = new TextField();
        operand2.setStyle("-fx-font-size:14pt; -fx-font-weight:normal; -fx-font-family:Century Gothic, sans-serif");
        operand2.setAlignment(Pos.CENTER_RIGHT);
        layout.add(operand2, 1, 2);

        // Column 0, Row 3
        Label resultLabel = new Label("Result:");
        resultLabel.setStyle("-fx-font-size:14pt; -fx-font-weight:bold; -fx-font-family:Century Gothic, sans-serif");
        layout.add(resultLabel, 0, 3);

        // Column 1, Row 3
        result = new TextField();
        result.setStyle("-fx-font-size:14pt; -fx-font-weight:normal; -fx-font-family:Century Gothic, sans-serif");
        result.setAlignment(Pos.CENTER_RIGHT);
        // Do not allow editing in the result control
        result.setEditable(false);
        layout.add(result, 1, 3);

        // Add Button
        Button add = new Button("+");
        add.setStyle("-fx-font-size:14pt; -fx-font-weight:bold; -fx-font-family:Century Gothic, sans-serif");
        // Subtract button
        Button subtract = new Button("-");
        subtract.setStyle("-fx-font-size:14pt; -fx-font-weight:bold; -fx-font-family:Century Gothic, sans-serif");
        // Multiply button
        Button multiply = new Button("*");
        multiply.setStyle("-fx-font-size:14pt; -fx-font-weight:bold; -fx-font-family:Century Gothic, sans-serif");
        // Divide button
        Button divide = new Button("/");
        divide.setStyle("-fx-font-size:14pt; -fx-font-weight:bold; -fx-font-family:Century Gothic, sans-serif");
        // Exit button
        Button exit = new Button("Exit");
        exit.setStyle("-fx-font-size:14pt; -fx-font-weight:bold; -fx-font-family:Century Gothic, sans-serif");

        // HBox that will span 2 columns so that buttons can be centered across the GridPane
        HBox hboxBtn = new HBox();
        // Add the buttons to the HBox
        hboxBtn.getChildren().addAll(add, subtract, multiply, divide, exit);
        // Center the contents of the HBox
        hboxBtn.setAlignment(Pos.CENTER);
        // Spacing around the controls in the HBox
        hboxBtn.setPadding(new Insets(20.0));
        // Add space between buttons
        hboxBtn.setSpacing(10.0); // Spacing around the controls in the HBox

        // Place the HBOX in column 0, row 4, spanning 2 columns and 1 row
        layout.add(hboxBtn, 0, 4, 2, 1);

        // Connect the buttons to their event handler
        add.setOnAction(this::addAction);
        subtract.setOnAction(this::subtractAction);
        multiply.setOnAction(this::multiplyAction);
        divide.setOnAction(this::divideAction);
        exit.setOnAction(this::exitAction);

        // Set the column widths as a percentage
        ColumnConstraints col1 = new ColumnConstraints();
        col1.setPercentWidth(30.0);
        ColumnConstraints col2 = new ColumnConstraints();
        col2.setPercentWidth(70.0);
        layout.getColumnConstraints().addAll(col1, col2);

        // Add space around the outside of the GridPane
        layout.setPadding(new Insets(0, 40, 0, 40));
        // Add space between rows and columns of the GridPane
        layout.setHgap(10.0);
        layout.setVgap(10.0);

        return layout;
    }

    /**
     * Addition : No check for valid strings for parseDouble
     *
     * @param event
     */
    private void addAction(ActionEvent event) {
        double op1 = Double.parseDouble(operand1.getText());
        double op2 = Double.parseDouble(operand2.getText());
        String ans = NumberFormat.getNumberInstance().format((op1 + op2));
        result.setText(ans);
    }

    /**
     * Subtraction : No check for valid strings for parseDouble
     *
     * @param event
     */
    private void subtractAction(ActionEvent event) {
        double op1 = Double.parseDouble(operand1.getText());
        double op2 = Double.parseDouble(operand2.getText());
        String ans = NumberFormat.getNumberInstance().format((op1 - op2));
        result.setText(ans);
    }

    /**
     * Multiplication : No check for valid strings for parseDouble
     *
     * @param event
     */
    private void multiplyAction(ActionEvent event) {
        double op1 = Double.parseDouble(operand1.getText());
        double op2 = Double.parseDouble(operand2.getText());
        String ans = NumberFormat.getNumberInstance().format((op1 * op2));
        result.setText(ans);
    }

    /**
     * Divide : No check for valid strings for parseDouble Check if operand2
     * equals zero before the division
     *
     * @param event
     */
    private void divideAction(ActionEvent event) {
        double op1 = Double.parseDouble(operand1.getText());
        double op2 = Double.parseDouble(operand2.getText());
        if (op2 != 0) {
            String ans = NumberFormat.getNumberInstance().format((op1 / op2));
            result.setText(ans);
        } else {
            result.setText("Invalid operand 2");
        }
    }

    /**
     * Use Platform.exit() rather than System.exit(0)
     *
     * @param event
     */
    private void exitAction(ActionEvent event) {
        Platform.exit();
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }

}
